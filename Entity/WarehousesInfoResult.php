<?php
namespace Ipol\Fivepost\Fivepost\Entity;

use Ipol\Fivepost\Api\Entity\Response\ErrorResponse;
use Ipol\Fivepost\Api\Entity\Response\WarehousesInfo as ObjResponse;

/**
 * Class WarehousesInfoResult
 * @package Ipol\Fivepost\Fivepost
 * @subpackage Entity
 * @method ObjResponse|ErrorResponse getResponse
 */
class WarehousesInfoResult extends AbstractResult
{
}